FROM python:3.8

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
WORKDIR /code/project

COPY Pipfile Pipfile.lock /code/project/

# copy and install pip requirements
RUN pip3 install pipenv && pipenv install --system

# copy Django project files
COPY ./src/ /code/project/
