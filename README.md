**Docker Django template**

## Env files
### .env :
#### Django
```
SECRET_KEY=
```
```
DEBUG=
```
```
ALLOWED_HOST=
```
#### Database
```
SQL_NAME=
```
```
SQL_USER=
```
```
SQL_PASSWORD=
```
```
SQL_HOST=
```
```
SQL_PORT=
```

### .env.db :
```
NAME=
```
```
USER=
```
```
PASSWORD=
```

### [Generate django secret key](https://djecrety.ir/)
### Change server_name in /config/nginx/conf.d/project.conf
